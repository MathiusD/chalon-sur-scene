# Châlon Sur Scène

Un [petit site](https://mathiusd.gitlab.io/chalon-sur-scene/) dans le contexte d'un jeu de piste sur Châlon organisé par [UnisCite](https://www.uniscite.fr/) et Alken. Le principe est d'afficher les indices avec le code de ces derniers.
