import { library } from '@fortawesome/fontawesome-svg-core';
import { faGitlab } from '@fortawesome/free-brands-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';

library.add(
    faGitlab
);

export {
    FontAwesomeIcon
};