import { createApp } from 'vue';
import App from './App.vue';

import { FontAwesomeIcon } from './config/fontAwesome.js';

import 'bulma/bulma.sass';

let app = createApp(App);

app.component('font-awesome-icon', FontAwesomeIcon);

app.mount('#app');
